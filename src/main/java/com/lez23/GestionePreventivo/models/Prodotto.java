package com.lez23.GestionePreventivo.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity
@Table(name="prodotti")
public class Prodotto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="prodottoID")
	private int id;
	
	@Column
	private String codice;
	@Column
	private String nome;
	@Column
	private int prezzo;
	@Column
	private String categoria;
	
	@ManyToMany
	@JoinTable(name = "preventivo_prodotto",
			joinColumns = { @JoinColumn(name="ProdottoIdRif", referencedColumnName = "EsameID") },
			inverseJoinColumns = { @JoinColumn(name="PreventivoIdRif", referencedColumnName = "PreventivoID") })
	private List<Preventivo> preventivi;
	
	public Prodotto() {
		
	}
	
	
	public Prodotto(int id, String codice, String nome, int prezzo, String categoria) {
		super();
		this.id = id;
		this.codice = codice;
		this.nome = nome;
		this.prezzo = prezzo;
		this.categoria = categoria;
	}


	public List<Preventivo> getPreventivi() {
		return preventivi;
	}


	public void setPreventivi(List<Preventivo> preventivi) {
		this.preventivi = preventivi;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getCodice() {
		return codice;
	}


	public void setCodice(String codice) {
		this.codice = codice;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public int getPrezzo() {
		return prezzo;
	}


	public void setPrezzo(int prezzo) {
		this.prezzo = prezzo;
	}


	public String getCategoria() {
		return categoria;
	}


	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	

}
