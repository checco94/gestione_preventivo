package com.lez23.GestionePreventivo.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity
@Table(name="preventivi")
public class Preventivo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="preventivoID")
	private int id;
	
	@Column
	private String intestatario;
	@Column
	private String indirizzo;
	
	@Column(insertable=false)
	private String data_preventivo;
	
	@ManyToOne
	@JoinColumn(name="ClienteRif")			
	private Cliente cliente;
	
	public Preventivo() {
		
	}
	
	@ManyToMany
	@JoinTable(name="preventivo_prodotto",
			joinColumns = { @JoinColumn(name="PreventivoIdRif", referencedColumnName = "PreventivoID") },
			inverseJoinColumns = { @JoinColumn(name="ProdottiIdRif", referencedColumnName = "ProdottoID") })
	private List<Prodotto> prodotti;
	
	
	public Preventivo(int id, String intestatario, String indirizzo, String data_preventivo) {
		super();
		this.id = id;
		this.intestatario = intestatario;
		this.indirizzo = indirizzo;
		this.data_preventivo = data_preventivo;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getIntestatario() {
		return intestatario;
	}


	public void setIntestatario(String intestatario) {
		this.intestatario = intestatario;
	}


	public String getIndirizzo() {
		return indirizzo;
	}


	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}


	public String getData_preventivo() {
		return data_preventivo;
	}


	public void setData_preventivo(String data_preventivo) {
		this.data_preventivo = data_preventivo;
	}


	public Cliente getCliente() {
		return cliente;
	}


	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}


	public List<Prodotto> getProdotti() {
		return prodotti;
	}


	public void setProdotti(List<Prodotto> prodotti) {
		this.prodotti = prodotti;
	}


	
	
}
