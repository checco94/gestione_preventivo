package com.lez23.GestionePreventivo.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ManyToAny;



@Entity
@Table(name="cliente")
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="clienteID")
	private int id;
	
	@Column
	private String nome_azienda;
	@Column
	private String intestatario;
	@Column
	private String indirizzo_azienda;
	
	@OneToMany(mappedBy = "preventivo")
	private List<Preventivo> elencoPreventivo;
	
	public Cliente() {
		this.elencoPreventivo = new ArrayList<Preventivo>();
		
	}
	
	public Cliente(int id, String nome_azienda, String intestatario, String indirizzo_azienda) {
		super();
		this.id = id;
		this.nome_azienda = nome_azienda;
		this.intestatario = intestatario;
		this.indirizzo_azienda = indirizzo_azienda;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome_azienda() {
		return nome_azienda;
	}

	public void setNome_azienda(String nome_azienda) {
		this.nome_azienda = nome_azienda;
	}

	public String getIntestatario() {
		return intestatario;
	}

	public void setIntestatario(String intestatario) {
		this.intestatario = intestatario;
	}

	public String getIndirizzo_azienda() {
		return indirizzo_azienda;
	}

	public void setIndirizzo_azienda(String indirizzo_azienda) {
		this.indirizzo_azienda = indirizzo_azienda;
	}

	public List<Preventivo> getElencoPreventivo() {
		return elencoPreventivo;
	}

	public void setElencoPreventivo(List<Preventivo> elencoPreventivo) {
		this.elencoPreventivo = elencoPreventivo;
	}


	
}
