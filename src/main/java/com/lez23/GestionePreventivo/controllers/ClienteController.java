package com.lez23.GestionePreventivo.controllers;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lez23.GestionePreventivo.models.Cliente;
import com.lez23.GestionePreventivo.services.ClienteService;


@RestController
@RequestMapping("/Clienti")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ClienteController {
	
	@Autowired
	private ClienteService service;

	@PostMapping("/insert")
	public Cliente inserisci_cliente(@RequestBody Cliente objCliente) {
		return service.insert(objCliente);
	}
	
	@GetMapping("/")
	public List<Cliente> lista_clienti(){
		return service.findAll();
	}
	
	
}
