package com.lez23.GestionePreventivo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lez23.GestionePreventivo.models.Prodotto;
import com.lez23.GestionePreventivo.services.ProdottoService;



@RestController
@RequestMapping("/prodotti")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ProdottoController {
	
	@Autowired
	private ProdottoService service;

	@PostMapping("/insert")
	public Prodotto seleziona_prodotto(@RequestBody Prodotto objPro) {
		return service.insert(objPro);
	}
	
	@GetMapping("/")
	public List<Prodotto> lista_prodotti(){
		return service.findAll();
	}
	
	
}
