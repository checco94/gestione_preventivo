package com.lez23.GestionePreventivo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lez23.GestionePreventivo.models.Preventivo;
import com.lez23.GestionePreventivo.services.PreventivoService;

@RestController
@RequestMapping("/Preventivi")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PreventivoController {
	
	@Autowired
	private PreventivoService service;

	@PostMapping("/insert")
	public Preventivo inserisci_preventivo(@RequestBody Preventivo objPrev) {
		return service.insert(objPrev);
	}
	
	@GetMapping("/")
	public List<Preventivo> lista_preventivi(){
		return service.findAll();
	}
	
	
}
