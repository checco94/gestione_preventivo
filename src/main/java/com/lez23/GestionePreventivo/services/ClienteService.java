package com.lez23.GestionePreventivo.services;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lez23.GestionePreventivo.models.Cliente;
import com.lez23.GestionePreventivo.repositories.DataAccessRepo;

@Service
public class ClienteService implements DataAccessRepo<Cliente> {

	@Autowired
	private EntityManager ent_man;
	
	private Session getSessione() {
		return ent_man.unwrap(Session.class);
	}
	
	@Override
	public Cliente insert(Cliente t) {

		Cliente temp = new Cliente();
		temp.setNome_azienda(t.getNome_azienda());
		temp.setIntestatario(t.getIntestatario());
		temp.setIndirizzo_azienda(t.getIndirizzo_azienda());
		
		Session sessione = this.getSessione();
		
		try {
			sessione.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return null;
	}

	@Override
	public List<Cliente> findAll() {
		Session sessione = getSessione();
		return sessione.createCriteria(Cliente.class).list();
	}

}
