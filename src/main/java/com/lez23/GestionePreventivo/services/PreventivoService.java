package com.lez23.GestionePreventivo.services;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lez23.GestionePreventivo.models.Preventivo;
import com.lez23.GestionePreventivo.repositories.DataAccessRepo;



@Service
public class PreventivoService implements DataAccessRepo<Preventivo> {

	@Autowired
	private EntityManager ent_man;
	
	private Session getSessione() {
		return ent_man.unwrap(Session.class);
	}
	
	@Override
	public Preventivo insert(Preventivo t) {

		Preventivo temp = new Preventivo();
		temp.setIntestatario(t.getIntestatario());
		temp.setIndirizzo(t.getIndirizzo());
		temp.setData_preventivo(t.getData_preventivo());
		
		Session sessione = this.getSessione();
		
		try {
			sessione.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return null;
	}

	@Override
	public List<Preventivo> findAll() {
		Session sessione = getSessione();
		return sessione.createCriteria(Preventivo.class).list();
	}

}
