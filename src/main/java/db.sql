DROP DATABASE IF EXISTS gestione_preventivo;
CREATE DATABASE gestione_preventivo;
USE gestione_preventivo;

CREATE TABLE cliente (
clienteId INTEGER NOT NULL AUTO_INCREMENT,
nome_azienda VARCHAR (50),
intestatario VARCHAR (50),
indirizzo_azienda VARCHAR (50),
PRIMARY KEY (clienteID)
);

CREATE TABLE preventivo (
preventivoId INTEGER NOT NULL AUTO_INCREMENT,
intestatario VARCHAR(50),
indirizzo VARCHAR(50),
data_preventivo DATETIME DEFAULT NOW(),
PRIMARY KEY (preventivoId)
);

CREATE TABLE prodotto (
prodottoId INTEGER NOT NULL AUTO_INCREMENT,
codice VARCHAR(50),
nome VARCHAR(50),
prezzo int,
categoria VARCHAR(50),
PRIMARY KEY (prodottoId)
);

CREATE TABLE cliente_preventivo (
ClienteIdRif INTEGER NOT NULL,
PreventivoIdRif INTEGER NOT NULL,
FOREIGN KEY (ClienteIdRif) REFERENCES cliente(clienteId),
FOREIGN KEY (PreventivoIdRif) REFERENCES preventivo(preventivoId),
UNIQUE (ClienteIdRif, PreventivoIdRif)
);

CREATE TABLE preventivo_prodotto (
PreventivoIdRif INTEGER NOT NULL,
ProdottoIdRif INTEGER NOT NULL,
FOREIGN KEY (PreventivoIdRif) REFERENCES preventivo(preventivoId),
FOREIGN KEY (ProdottoIdRif) REFERENCES prodotto(prodottoId),
UNIQUE (PreventivoIdRif, ProdottoIdRif)
);

INSERT INTO cliente (nome_azienda, intestatario, indirizzo_azienda) VALUES
("Mercedes", "Marco Rossi", "Roma 80"),
("Audi", "Matteo Rossi", "Milano 21");

INSERT INTO preventivo (intestatario, indirizzo ) VALUES
("Marco Rossi", "Roma 80"),
("Matteo Rossi", "Milano 21");


INSERT INTO cliente_preventivo(ClienteIdRif , PreventivoIdRif) VALUES
(1, 1),
(2, 2),
(1, 2),
(2, 1);


INSERT INTO prodotto (codice , nome , prezzo , categoria ) VALUES
("ABC123", "Unipol", "250", "Assicurazione"),
("BC456", "Allianz", "180","Assicurazione");


INSERT INTO preventivo_prodotto(PreventivoIdRif, ProdottoIdRif) VALUES
(1, 1),
(2, 2),
(2, 1),
(1, 2);


SELECT * FROM cliente
JOIN cliente_preventivo ON cliente.clienteId = cliente_preventivo.ClienteIdRif
JOIN preventivo ON cliente_preventivo.PreventivoIdRif = preventivo.PreventivoId ;


SELECT intestatario
FROM preventivo
JOIN cliente_preventivo ON preventivo.preventivoId = cliente_preventivo.PreventivoIdRif
JOIN cliente ON cliente_preventivo.ClienteIdRif = cliente.ClienteId
WHERE nome_azienda = "Mercedes";

SELECT * FROM preventivo
JOIN preventivo_prodotto ON preventivo.preventivoId = preventivo_prodotto.PreventivoIdRif
JOIN prodotto ON preventivo_prodotto.ProdottoIdRif = prodotto.ProdottoId
WHERE codice = "ABC123";

SELECT prodotto.nome
FROM cliente
JOIN cliente_preventivo on cliente.clienteId = cliente_preventivo.ClienteIdRif
JOIN preventivo on cliente_preventivo.PreventivoIdRif = preventivo.preventivoId
JOIN preventivo_prodotto on preventivo.preventivoId = preventivo_prodotto.PreventivoIdRif
JOIN prodotto on preventivo_prodotto.ProdottoIdRif = prodotto.prodottoId
WHERE cliente.nome_azienda = "Mercedes";